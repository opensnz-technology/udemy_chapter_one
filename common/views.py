from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import exceptions
from .serializers import UserSerializer
from core.models import User
from .authentification import JWTAuthentification


class RegisterAPIView(APIView):
    def post(self, request):
        data = request.data
        if data['password'] != data['password_confirm']:
            raise exceptions.APIException("Password n'est pas identiques")
        data['is_admin'] = 0
        serializer = UserSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class LoginAPIView(APIView):

    def post(self, request):

        email = request.data['email']
        password = request.data['password']
        user = User.objects.filter(email=email).first()

        if user is None:
            raise exceptions.AuthenticationFailed('user not found')
        if not user.check_password(password):
            raise exceptions.AuthenticationFailed('Password is wrong')

        token = JWTAuthentification.generate_jwt(user.id)
        response = Response()
        response.set_cookie(key='jwt', value=token, httponly=True)
        response.data = {
            'message': 'Success'
        }
        return response


class UserAPIView (APIView):
    authentication_classes = [JWTAuthentification]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return Response(UserSerializer(request.user).data)


class LogoutAPIView (APIView):

    authentication_classes = [JWTAuthentification]
    permission_classes = [IsAuthenticated]

    def post(self, _):
        response = Response()
        response.delete_cookie(key='jwt')
        response.data = {
            'message': "Logout Succees"
        }
        return response


class ProfileUpdateAPIView (APIView):

    authentication_classes = [JWTAuthentification]
    permission_classes = [IsAuthenticated]

    def put(self, request, pk=None):
        user = request.user
        serializer = UserSerializer(user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class PasswordUpdateAPIView (APIView):

    authentication_classes = [JWTAuthentification]
    permission_classes = [IsAuthenticated]

    def put(self, request, pk=None):
        user = request.user
        data = request.data
        if data['password'] != data['password_confirm']:
            raise exceptions.APIException("Password n'est pas identiques")
        user.set_password(data['password'])
        user.save()
        return Response(UserSerializer(request.user).data)