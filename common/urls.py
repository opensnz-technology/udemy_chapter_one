from django.urls import path
from .views import RegisterAPIView
from .views import LoginAPIView
from .views import LogoutAPIView
from .views import UserAPIView
from .views import ProfileUpdateAPIView
from .views import PasswordUpdateAPIView

urlpatterns = [
    path('register', RegisterAPIView.as_view()),
    path('login', LoginAPIView.as_view()),
    path('user', UserAPIView.as_view()),
    path('logout', LogoutAPIView.as_view()),
    path('users/update', ProfileUpdateAPIView.as_view()),
    path('users/password', PasswordUpdateAPIView.as_view()),
]
